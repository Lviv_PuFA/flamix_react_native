import React from 'react';
import styled from 'styled-components/native';
import {useFonts} from 'expo-font';

const AnaliticItem = ({title,svg,text,navigate}) => {
  const [loaded] = useFonts({
    Montserrat: require('../assets/fonts/Montserrat-Medium.ttf'),
  });

  if (!loaded) {
    return null;
  }
  return (
    <Conteiner>
      <GroupItem style={{
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 3,
        }}
        onPress={() => navigate(title)}>
        <GroupIcon>
          {svg}
        </GroupIcon>
        <GroupTitle>
          {title}
        </GroupTitle>
        <GroupText>
          {text}
        </GroupText>
      </GroupItem> 
    </Conteiner>
  )
};

const Conteiner = styled.View`

  flex: 1;
  margin-left:5%;

`;

const GroupItem = styled.TouchableOpacity`

  width: 155px;
  height: 189px;
  background: #FFFFFF;
  box-shadow: 0px 4px 4px rgba(1, 154, 222, 0.15);
  border-radius: 10px;
  align-items: center;
  justify-content: center;

`;
const GroupIcon = styled.Text`

  text-align: center;

`;
const GroupTitle = styled.Text`

  font-weight: 600;
  font-size: 14px;
  line-height: 17px;
  text-align: center;
  color: #252733;
  font-family:Montserrat;

`;
const GroupText = styled.Text`

  font-weight: normal;
  font-size: 8px;
  line-height: 12px;
  text-align: center;
  color: #252733;
  padding:0 10%;
  font-family:Montserrat;
`;

export default AnaliticItem;
