import React from 'react';
import styled from 'styled-components/native';
import AnaliticItem from './AnaliticItem';
import Budget from '../Icons/Budget'
import CashFlow from '../Icons/CashFlow';
import Creditor from '../Icons/Creditor';
import PL from '../Icons/PL';
import Receivables from '../Icons/Receivables';
import {Button,ScrollView} from 'react-native';

 const AnaliticPage = ({navigate}) => {
  return (
    <Conteiner>
      <ScrollView>
      <Rows>
      <AnaliticItem title='CashFlow' svg={<CashFlow/>} text='Отчет о движении денежных средств' navigate={navigate}/>
      <AnaliticItem title='P&L' svg={<PL/>} text='Отчет о прибыли и убытков' navigate={navigate}/>
      </Rows>
      <Rows>
      <AnaliticItem title='Budgeting' svg={<Budget/>} text='Планирование и разработка бюджетов' navigate={navigate}/>
      <AnaliticItem title='Creditor' svg={<Creditor/>} text='Развернутая кредиторска задолженность' navigate={navigate}/>
      </Rows>
      <Rows>
      <AnaliticItem title='Receivables' svg={<Receivables/>} text='Развернутая дебиторская задолженность' navigate={navigate}/>
      </Rows>
      </ScrollView>
    </Conteiner>
  );
}

const Conteiner = styled.View`

    flex: 1;

`;
const Rows = styled.View`

    flex-direction: row;
    margin:5% 0 0 0; 
    padding: 0 0 5% 0 ;
    
`;

export default AnaliticPage;
