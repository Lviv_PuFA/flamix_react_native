import React from 'react'; 
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './Screens/Home'

import Analitic from './Screens/Analitic'
import Transactions from './Screens/Transactions'
import Stats from './Screens/Stats'

import CashFlowInfo from './InfoComponents/CashFlowInfo'
import PLInfo from './InfoComponents/PLInfo'
import BudgetInfo from './InfoComponents/BudgetInfo'
import CreditorInfo from './InfoComponents/CreditorInfo'
import ReceivablesInfo from './InfoComponents/ReceivablesInfo'

import PersonalSettings from './Screens/PersonalSettings'


const Stack = createStackNavigator();

 const App = () => {
  
  return (

    <NavigationContainer>
    <Stack.Navigator>
    <Stack.Screen name="Home" component={Home} options={{
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} />
      <Stack.Screen name="Analitic" component={Analitic} />
      <Stack.Screen name="Transactions" component={Transactions} />
      <Stack.Screen name="Stats" component={Stats} />

      <Stack.Screen name="CashFlow" component={CashFlowInfo} />
      <Stack.Screen name="P&L" component={PLInfo} />
      <Stack.Screen name="Budgeting" component={BudgetInfo} />
      <Stack.Screen name="Creditor" component={CreditorInfo} />
      <Stack.Screen name="Receivables" component={ReceivablesInfo} />

      <Stack.Screen name="PersonalSettings" component={PersonalSettings} />
    </Stack.Navigator>
  </NavigationContainer>
   
  );
}
  
export default App;
