import React from 'react';
import Svg, { Path } from "react-native-svg";

const PL= (props) => {
  return (
    <Svg width={47} height={35} viewBox="0 0 47 35" fill="none" {...props}>
      <Path
        d="M40.657 18.887V7.672a.914.914 0 00-.915-.917H30.14a.914.914 0 00-.915.917v11.215a.914.914 0 01-.916.918h-4.924c-.508 0-.657.319-.349.718l5.661 7.084L34.36 34.7c.319.399.836.399 1.145 0l5.67-7.094 5.662-7.084c.318-.399.159-.718-.349-.718h-4.914a.92.92 0 01-.916-.918zM12.641.299c-.319-.399-.836-.399-1.145 0l-5.67 7.094-5.662 7.084c-.318.399-.159.718.349.718h4.914c.508 0 .916.41.916.918v11.215c0 .508.408.917.915.917h9.601a.914.914 0 00.915-.918V16.123c0-.509.408-.918.916-.918h4.915c.507 0 .666-.319.348-.718l-5.661-7.084L12.64.3z"
        fill="#019ADE"
      />
    </Svg>
  )
}

export default PL;
