import React from 'react';
import styled from 'styled-components/native';
import { View,Text,Button } from 'react-native';

const Home = ({navigation}) => {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>Home Screen</Text>
          <Button
            title="Go to My Analitic"
            onPress={() => navigation.navigate('Analitic')}
          />
          <Text>...</Text>
           <Button
            title="Go to My PersonalSettings"
            onPress={() => navigation.navigate('PersonalSettings')}
          />
        </View>
      );
  }
  const Info = styled.TouchableOpacity`
      background:green;
      width:100px;
      height:100px;
     
  `;

    
  
  export default Home;
  