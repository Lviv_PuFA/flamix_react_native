import React from 'react';
import { View, Text, TextInput, StyleSheet, } from 'react-native'
import styled from 'styled-components/native';
import Button from '../src/Button';
import Input from '../src/Input';



const PersonalSettings = () => {
    return (
        <Conteiner>
            <InputCase>
            <Input title='Name' placeholder='Enter Name' />
            <Input title='Email' placeholder='Enter Email' />
            <Input title='Password' placeholder='Enter Password' secure={true}/>
            <Input title='Confirm password' placeholder='Confirm password' secure={true}/>
            </InputCase>
            <ButtonCase>
                 <Button title='Save' />
            </ButtonCase>
        </Conteiner>
    
       
    )
}

const Conteiner = styled.View`
   
`;
const InputCase= styled.View`
    margin: 2% 0 0 5%
`
const ButtonCase= styled.View`
    flex:1;
    margin: 10% 0 0 0;
    align-items:center
`

export default PersonalSettings
