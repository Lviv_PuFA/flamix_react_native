import React from 'react'
import styled from 'styled-components/native';
import { useFonts } from 'expo-font';

const Button = ({title,action}) => {
    const [loaded] = useFonts({
        Montserrat: require('../assets/fonts/Montserrat-Medium.ttf'),
      });
    
      if (!loaded) {
        return null;
      }
    return (
        <Conteiner>
            <ButtonCase style={{
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 3,
        }} onPress={action}>
                <Text>{title}</Text>
            </ButtonCase>
        </Conteiner>
    )
}

const Conteiner = styled.View`

`
const ButtonCase= styled.TouchableOpacity`
background: #109CF1;
    height:45px;
    width:200px;
    border-radius:8px;
    align-items: center;
  justify-content: center;
  
`
const Text= styled.Text`

font-weight: 500;
font-size: 14px;
line-height: 20px;
    color: #fff;
    font-family:Montserrat;
`

export default Button
