import React from 'react';
import {  TextInput, StyleSheet, } from 'react-native'
import styled from 'styled-components/native';
import { useFonts } from 'expo-font';

const Input = ({secure, title, placeholder}) => {
  const [loaded] = useFonts({
    Montserrat: require('../assets/fonts/Montserrat-Bold.ttf'),
    
  });

  if (!loaded) {
    return null;
  }
    return (
      <Conteiner>
        <Text>{title}</Text>
        <InputCase >
        <TextInput secureTextEntry={secure} style={{...styles.input,fontFamily:'Montserrat'}} placeholder={placeholder}></TextInput>
        </InputCase>
      </Conteiner>
     
  )
}

Input.defaultProps = {
    secure: false,
  }

const Conteiner = styled.View`
margin-top:20px;
`;
const InputCase = styled.View`
margin:5px 0 0 0;
border: solid 1px #DFE0EB;
border-radius: 8px;
width:90%
height: 50px;
justify-content:center;

`
const Text= styled.Text`

    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    font-family:Montserrat;
`

const styles = StyleSheet.create({
    input: {
      height:50,
      fontWeight: '500',
      fontSize: 14,
      paddingLeft: 10
    }
})

export default Input
