import React from 'react';
import Svg, { Path } from "react-native-svg";

const Platform = (props) => {
  return (
    <Svg width={675} height={60} viewBox="0 0 675 60" fill="none" {...props}>
    <Path
      d="M0 0h273.5c33 0 38.5 28 63.5 28s28-28 56.5-28H675v60H0V0z"
      fill="#fff"
    />
  </Svg>
  )
}

export default Platform;
