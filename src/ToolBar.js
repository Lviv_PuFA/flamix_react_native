import React, { useState } from 'react';
import styled from 'styled-components/native';
import { AntDesign } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons'; 
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { SimpleLineIcons } from '@expo/vector-icons';
import { TouchableOpacity,
  StyleSheet,
  Modal,
  Text,
  TouchableHighlight,
  View } from 'react-native';
import Platform from './Platform'


const ToolBar = ({trans,stats,analit,navigate}) => {
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <Conteiner>
      <Plat style={{transform: [{translateX: -332.5}] }}> 
      <Platform/>
      </Plat>
      <TouchableOpacity style={styles.buttons} onPress={() => navigate('Transactions')}>
      <Feather name="credit-card" size={24} color={trans} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.buttons} onPress={() => navigate('Analitic')}>
      <Feather name="bar-chart-2" size={24} color={analit} />
      </TouchableOpacity>
      <Temp/>    
      <TouchableOpacity style={styles.buttons} onPress={() => navigate('Stats')}>
      <SimpleLineIcons style={styles.equalizer} name="equalizer" size={24} color={stats} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.buttons} onPress={() => navigate('Home')}>
      <MaterialCommunityIcons name="logout" size={24} color="black" /> 
      </TouchableOpacity>
      <PlusButton style={{shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 5}} onPress={() => {
          setModalVisible(!modalVisible);
        }}>
      <AntDesign name="plus" size={24} color="#fff" />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Hello! I am your PlusButton</Text>

            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <Text style={styles.textStyle}>Hide Modal</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      </PlusButton>
      
        </Conteiner>
  )
}

ToolBar.defaultProps = {
  trans: 'black',
  stats: 'black',
  analit: 'black'
}
const Temp = styled.View`

`;
const Plat = styled.View`
position:absolute;
left:50%;
`;

const Conteiner = styled.View`

  flex-direction: row;
  height: 60px;
  opacity:1;
  padding: 0 0 0 0; 
  background-color:transparent;
  justify-content: space-around;
 
`;

const PlusButton = styled.TouchableOpacity`
  width: 45px;
  height: 45px;
  border-radius: 50px;
  align-items: center;
  justify-content: center;
  position:absolute;
  bottom:37px; 
  left:45%; 
  background: #019ADE;
 
`;

const styles = StyleSheet.create({
  absolute: {
    flex: 1,
    position:'absolute'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  buttons:{
    width:70,
    height:60,
    justifyContent:'center',
    alignItems:'center'
  },
  equalizer:{
    transform:[{ rotate: '270deg'}]
  }
});

export default ToolBar;
